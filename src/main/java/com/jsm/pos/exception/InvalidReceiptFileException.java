package com.jsm.pos.exception;

/**
 * Provides notification that an invalid receipt file was encountered
 * 
 * @author josh
 *
 */
public class InvalidReceiptFileException extends RuntimeException {
  private static final long serialVersionUID = 3609803181128201938L;

  public InvalidReceiptFileException() {
    super();
  }

  public InvalidReceiptFileException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public InvalidReceiptFileException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidReceiptFileException(String message) {
    super(message);
  }

  public InvalidReceiptFileException(Throwable cause) {
    super(cause);
  }

}
