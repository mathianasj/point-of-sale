package com.jsm.pos.exception;

import java.util.Set;
import javax.validation.ConstraintViolation;
import com.jsm.pos.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Raised whenever some form of constraint was not me on an entity class
 * 
 * @author josh
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ValidationException extends RuntimeException {
  private final Class<? extends BaseEntity> clazz;
  private final Set<ConstraintViolation<BaseEntity>> failures;

  public ValidationException(Class<? extends BaseEntity> clazz,
      Set<ConstraintViolation<BaseEntity>> failures) {
    super(failures.toString());
    this.clazz = clazz;
    this.failures = failures;
  }

  private static final long serialVersionUID = 9061480195658943467L;

}
