package com.jsm.pos.entity;

/**
 * provides a way to get a unique identifier for an entity, useful in the future if the entities are
 * used with spring data
 * 
 * @author josh
 *
 * @param <T>
 */
public interface Identifiable<T> {
  /**
   * get the entities unique identifier
   * 
   * @return
   */
  public T getId();
}
