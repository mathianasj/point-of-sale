/**
 * 
 */
package com.jsm.pos.entity;

import java.util.Set;
import java.util.UUID;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import com.jsm.pos.exception.ValidationException;
import lombok.Data;

/**
 * Abstract class that provides identifiable entities and a validation function based on javax
 * annotations. The Identifiable is mainly to support future uses such as using spring data rest to
 * store the entities in a database
 * 
 * @author josh
 *
 */
@Data
public abstract class BaseEntity implements Identifiable<UUID>, Validatable {
  /**
   * Unique identifier of entity
   */
  private UUID id;

  /**
   * TODO there is a better way to do this, but it works for demonstration purposes I would refactor
   * this to not have to be instantiated here, and would like to do more research on how the best
   * way to handle this while following a Domain Driven Design methodology
   */
  @Override
  public void validate() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();

    Set<ConstraintViolation<BaseEntity>> failures = validator.validate(this);

    if (failures.size() > 0) {
      throw new ValidationException(this.getClass(), failures);
    }
  }
}
