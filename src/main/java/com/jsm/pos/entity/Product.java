package com.jsm.pos.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

/**
 * A product that can be sold and all required pricing and tax rates
 * 
 * @author josh
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor
@AllArgsConstructor
public class Product extends BaseEntity {
  private String title;
  @NotNull
  private BigDecimal price;
  private List<TaxRate> taxRates = Collections.<TaxRate>emptyList();

  /**
   * calculate the aggregate sum of all tax rates defined for the product
   * 
   * @return
   */
  public BigDecimal getSalesTax() {
    return taxRates.stream().map(taxRate -> {
      // i would like input arguments why this logic should be in tax rate instead, originally it
      // felt like it should go here, but am open to discussion
      taxRate.validate();
      return this.round(taxRate.getTaxRate().multiply(this.price), taxRate.getRoundingMultiplier(),
          RoundingMode.UP);
    }).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
  }

  // TODO this should be broken out into another class, it is not following the S of SOLID design to
  // be here
  private BigDecimal round(BigDecimal value, BigDecimal increment, RoundingMode roundingMode) {
    if (increment.signum() == 0) {
      // 0 increment does not make much sense, but prevent division by 0
      return value;
    } else {
      BigDecimal divided = value.divide(increment, 0, roundingMode);
      BigDecimal result = divided.multiply(increment);
      return result;
    }
  }
}
