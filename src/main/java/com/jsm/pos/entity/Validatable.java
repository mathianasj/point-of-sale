package com.jsm.pos.entity;

/**
 * defines an interface that provides self validation
 * 
 * @author josh
 *
 */
public interface Validatable {
  void validate();
}
