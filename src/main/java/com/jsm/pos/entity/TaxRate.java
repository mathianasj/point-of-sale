package com.jsm.pos.entity;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * defines a tax rate for calculating taxes on a receipt or for a product
 * 
 * @author josh
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class TaxRate extends BaseEntity {
  @NotNull
  private BigDecimal taxRate;
  private String title;
  private BigDecimal roundingMultiplier = new BigDecimal("0.05");

  public TaxRate(BigDecimal taxRate, String title) {
    this.taxRate = taxRate;
    this.title = title;
  }
}
