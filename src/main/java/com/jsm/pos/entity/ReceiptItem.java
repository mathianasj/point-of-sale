package com.jsm.pos.entity;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

/**
 * Allows a single line item on a receipt that includes the product and number of items purchased
 * 
 * @author josh
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@RequiredArgsConstructor
public class ReceiptItem extends BaseEntity {
  @NotNull
  private Integer qty;
  @NotNull
  private Product product;

  /**
   * get subtotal for the product and quantity
   * 
   * @return
   */
  public BigDecimal getSubTotal() {
    this.validate();
    this.product.validate();

    return product.getPrice().multiply(new BigDecimal(qty));
  }

  /**
   * get all taxes for this product and quantity
   * 
   * @return
   */
  public BigDecimal getTax() {
    this.validate();
    this.product.validate();

    return product.getSalesTax().multiply(new BigDecimal(qty));
  }

  /**
   * get total include quantity purchased and applicable taxes
   * 
   * @return
   */
  public BigDecimal getTotal() {
    return this.getSubTotal().add(this.getTax());
  }
}
