package com.jsm.pos.entity;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * High level organizer that allows creating a receipt and populating it with line items
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Receipt extends BaseEntity {
  @NotNull
  private List<ReceiptItem> items;

  /**
   * Get the subtotal of all aggregate items before calculation of tax
   * 
   * @return
   */
  public BigDecimal getSubTotal() {
    this.validate();
    return items.stream().map(i -> i.getSubTotal()).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
  }

  /**
   * get the aggregate sum tax of all items
   * 
   * @return
   */
  public BigDecimal getTax() {
    this.validate();
    return items.stream().map(i -> i.getTax()).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
  }

  /**
   * get the aggregate total of all items including product cost of all quantities purchased and
   * applicable taxes
   * 
   * @return
   */
  public BigDecimal getTotal() {
    this.validate();
    return items.stream().map(i -> i.getTotal()).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
  }
}
