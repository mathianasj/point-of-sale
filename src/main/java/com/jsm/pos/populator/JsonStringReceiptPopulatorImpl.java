package com.jsm.pos.populator;

import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.pos.entity.Receipt;
import lombok.RequiredArgsConstructor;

/**
 * Provides a way to load a json string and convert it to a Receipt
 * 
 * @author josh
 *
 */
@RequiredArgsConstructor
public class JsonStringReceiptPopulatorImpl implements ReceiptPopulator<String> {
  private final ObjectMapper mapper;

  @Override
  public Receipt populate(String input) {
    try {
      return mapper.readValue(input, Receipt.class);
    } catch (IOException ex) {
      throw new RuntimeException("could not read json receipt", ex);
    }
  }

}
