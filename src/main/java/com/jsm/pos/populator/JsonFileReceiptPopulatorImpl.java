package com.jsm.pos.populator;

import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.pos.entity.Receipt;
import com.jsm.pos.exception.InvalidReceiptFileException;
import lombok.RequiredArgsConstructor;

/**
 * provides a way to load a json file as a receipt
 * 
 * @author josh
 *
 */
@RequiredArgsConstructor
public class JsonFileReceiptPopulatorImpl implements ReceiptPopulator<File> {
  private final ObjectMapper objectMapper;

  @Override
  public Receipt populate(File input) {
    try {
      return objectMapper.readValue(input, Receipt.class);
    } catch (IOException ex) {
      throw new InvalidReceiptFileException("input file is invalid", ex);
    }
  }

}
