package com.jsm.pos.populator;

import com.jsm.pos.entity.Receipt;

/**
 * defines a way to create a receipt from an input
 * 
 * @author josh
 *
 * @param <I>
 */
public interface ReceiptPopulator<I> {
  /**
   * Converts the input to a Receipt
   * 
   * @param input
   * @return
   */
  public Receipt populate(I input);
}
