package com.jsm.pos.formatter;

/**
 * used to define formatters to convert an object to a string
 * 
 * @author josh
 *
 * @param <T>
 */
public interface Formatter<T> {
  /**
   * Converts the input object to a given formatted string
   * 
   * @param input
   * @return
   */
  public String format(T input);
}
