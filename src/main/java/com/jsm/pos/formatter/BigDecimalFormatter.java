package com.jsm.pos.formatter;

import java.math.BigDecimal;

/**
 * provides a mean to convert a BigDecimal into a string for printing
 * 
 * @author josh
 *
 */
public interface BigDecimalFormatter extends Formatter<BigDecimal> {

}
