package com.jsm.pos.formatter;

import java.io.PrintStream;
import java.util.List;
import com.jsm.pos.entity.Receipt;
import com.jsm.pos.entity.ReceiptItem;
import lombok.RequiredArgsConstructor;

/**
 * Converts a receipt object into a printable receipt
 * 
 * @author josh
 *
 */
@RequiredArgsConstructor
public class CommandLineFormatter implements ReceiptFormatter {
  private final PrintStream printStream;
  private final BigDecimalFormatter bigDecimalFormatter;

  /**
   * Writes out the receipt to the instances printStream using the instances bigDecimalFormatter for
   * currency
   */
  @Override
  public void write(Receipt receipt) {
    this.writeProductLines(receipt.getItems());
    this.writeSalesTax(receipt);
    this.writeTotal(receipt);
  }

  private void writeProductLines(List<ReceiptItem> receiptItems) {
    receiptItems.forEach(receiptItem -> this.writeProductLine(receiptItem));
  }

  private void writeProductLine(ReceiptItem receiptItem) {
    printStream.println(receiptItem.getQty() + " " + receiptItem.getProduct().getTitle() + ": "
        + bigDecimalFormatter.format(receiptItem.getTotal()));
  }

  private void writeSalesTax(Receipt receipt) {
    printStream.println("Sales Taxes: " + bigDecimalFormatter.format(receipt.getTax()));
  }

  private void writeTotal(Receipt receipt) {
    printStream.println("Total: " + bigDecimalFormatter.format(receipt.getTotal()));
  }
}
