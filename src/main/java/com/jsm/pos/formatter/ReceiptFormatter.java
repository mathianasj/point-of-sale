package com.jsm.pos.formatter;

import com.jsm.pos.entity.Receipt;

/**
 * defines a way to write a receipt
 * 
 * @author josh
 *
 */
public interface ReceiptFormatter {
  /**
   * Write the provided receipt to the implementing format
   * 
   * @param receipt
   */
  public void write(Receipt receipt);
}
