package com.jsm.pos.formatter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Implementation to output a BigDecimal as a currency string
 * 
 * @author josh
 *
 */
@AllArgsConstructor
@NoArgsConstructor
public class BigDecimalFormatterImpl implements BigDecimalFormatter {
  // setup to allow for future use of IOC/Spring
  private Integer scale = 2;
  private String format = "%,.2f";

  /**
   * Converts the input to the instances format rounding up the input as RoundingMode.HALF_UP
   * 
   * @see RoundingMode.HALF_UP
   */
  @Override
  public String format(BigDecimal input) {
    return String.format(format, input.setScale(scale, RoundingMode.HALF_UP));
  }

}
