package com.jsm.pos;

import java.io.File;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.pos.entity.Receipt;
import com.jsm.pos.formatter.BigDecimalFormatterImpl;
import com.jsm.pos.formatter.CommandLineFormatter;
import com.jsm.pos.formatter.ReceiptFormatter;
import com.jsm.pos.populator.JsonFileReceiptPopulatorImpl;
import com.jsm.pos.populator.ReceiptPopulator;
import lombok.RequiredArgsConstructor;

/**
 * Allows running all business logic to produce a receipt based on the input json containing all
 * receipt items, products, appropriate prices, quantities, and tax rates
 * 
 * @author josh
 *
 */
@RequiredArgsConstructor
public class CommandLineApplication {
  private final ReceiptPopulator<File> receiptPopulator;
  private final ReceiptFormatter receiptFormatter;

  public static void main(String[] args) {
    if (args.length != 1) {
      System.out.println("Point of Sale v1.0");
      System.out.println("    usage: CommandLineApp receipt.json");
    } else {
      new CommandLineApplication(new JsonFileReceiptPopulatorImpl(new ObjectMapper()),
          new CommandLineFormatter(System.out, new BigDecimalFormatterImpl())).write(args[0]);
    }
  }

  public void write(String fileName) {
    File file = new File(fileName);
    Receipt receipt = receiptPopulator.populate(file);

    // write receipt using formatter
    receiptFormatter.write(receipt);
  }
}
