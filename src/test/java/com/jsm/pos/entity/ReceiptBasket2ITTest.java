package com.jsm.pos.entity;

import java.io.File;
import java.math.BigDecimal;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class ReceiptBasket2ITTest extends ReceiptITTest {
  public ReceiptBasket2ITTest() {
    super(new BigDecimal("2250.80"), new BigDecimal("15012.25"), new BigDecimal("17263.05"),
        new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket2.json").getFile()));
  }
}
