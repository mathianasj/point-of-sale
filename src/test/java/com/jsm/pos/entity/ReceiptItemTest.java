package com.jsm.pos.entity;

import static com.jsm.pos.entity.TestUtil.assertValidation;
import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import com.jsm.pos.exception.ValidationException;

@PrepareForTest({ReceiptItem.class, Product.class})
public class ReceiptItemTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void qtyRequired() {
    ReceiptItem underTest = new ReceiptItem();

    assertValidation(underTest, "qty", TestUtil.NOT_NULL);
  }

  @Test
  public void productRequired() {
    ReceiptItem underTest = new ReceiptItem();

    assertValidation(underTest, "product", TestUtil.NOT_NULL);
  }

  @Test
  public void getSubtotalRequiresValidReceiptItem() {
    ReceiptItem underTest = new ReceiptItem();

    thrown.expect(ValidationException.class);

    underTest.getSubTotal();
  }

  @Test
  public void getSubtotalRequiresValidProduct() {
    ReceiptItem underTest = new ReceiptItem();
    Product product = PowerMockito.mock(Product.class);

    underTest.setQty(2);
    underTest.setProduct(product);

    Mockito.doThrow(new RuntimeException("product invalid")).when(product).validate();

    thrown.expect(Exception.class);
    thrown.expectMessage("product invalid");

    underTest.getSubTotal();
  }


  @Test
  public void getTaxRequiresValidReceiptItem() {
    ReceiptItem underTest = new ReceiptItem();

    thrown.expect(ValidationException.class);

    underTest.getTax();
  }

  @Test
  public void getTaxRequiresValidProduct() {
    ReceiptItem underTest = new ReceiptItem();
    Product product = PowerMockito.mock(Product.class);

    underTest.setQty(2);
    underTest.setProduct(product);

    Mockito.doThrow(new RuntimeException("product invalid")).when(product).validate();

    thrown.expect(Exception.class);
    thrown.expectMessage("product invalid");

    underTest.getTax();
  }


  @Test
  public void getTotalRequiresValidReceiptItem() {
    ReceiptItem underTest = new ReceiptItem();

    thrown.expect(ValidationException.class);

    underTest.getTotal();
  }

  @Test
  public void getTotalRequiresValidProduct() {
    ReceiptItem underTest = new ReceiptItem();
    Product product = PowerMockito.mock(Product.class);

    underTest.setQty(2);
    underTest.setProduct(product);

    Mockito.doThrow(new RuntimeException("product invalid")).when(product).validate();

    thrown.expect(Exception.class);
    thrown.expectMessage("product invalid");

    underTest.getTotal();
  }

  @Test
  public void getSubTotalMultipliesProductPriceByQty() {
    Product product = PowerMockito.mock(Product.class);
    ReceiptItem underTest = new ReceiptItem(2, product);

    Mockito.when(product.getPrice()).thenReturn(new BigDecimal("2.2"));

    BigDecimal actual = underTest.getSubTotal();
    BigDecimal expected = new BigDecimal("4.4");

    assertEquals(expected, actual);
  }

  @Test
  public void getTaxMultipliesProductTaxByQty() {
    Product product = PowerMockito.mock(Product.class);
    ReceiptItem underTest = new ReceiptItem(2, product);

    Mockito.when(product.getSalesTax()).thenReturn(new BigDecimal("1.2"));

    BigDecimal actual = underTest.getTax();
    BigDecimal expected = new BigDecimal("2.4");

    assertEquals(expected, actual);
  }

  @Test
  public void getTotalSumsSubtotalAndTax() {
    Product product = PowerMockito.mock(Product.class);
    ReceiptItem underTest = new ReceiptItem(2, product);

    Mockito.when(product.getPrice()).thenReturn(new BigDecimal("2.2"));
    Mockito.when(product.getSalesTax()).thenReturn(new BigDecimal("1.2"));

    BigDecimal actual = underTest.getTotal();
    BigDecimal expected = new BigDecimal("6.8");

    assertEquals(expected, actual);
  }
}
