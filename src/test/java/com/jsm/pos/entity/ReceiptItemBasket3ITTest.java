package com.jsm.pos.entity;

import java.io.File;
import java.math.BigDecimal;
import org.junit.Test;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class ReceiptItemBasket3ITTest extends ReceiptItemBasketITTest {

  public ReceiptItemBasket3ITTest() {
    super(new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket3.json").getFile()));
  }

  @Test
  public void testItem1SubTotal() {
    BigDecimal expected = new BigDecimal("75.99");

    this.testItemSubTotal(0, expected);
  }

  @Test
  public void testItem2SubTotal() {
    BigDecimal expected = new BigDecimal("55.00");

    this.testItemSubTotal(1, expected);
  }

  @Test
  public void testItem3SubTotal() {
    BigDecimal expected = new BigDecimal("10.00");

    this.testItemSubTotal(2, expected);
  }

  @Test
  public void testItem4SubTotal() {
    BigDecimal expected = new BigDecimal("997.99");

    this.testItemSubTotal(3, expected);
  }

  @Test
  public void testItem1Total() {
    BigDecimal expected = new BigDecimal("79.79");

    this.testItemTotal(0, expected);
  }

  @Test
  public void testItem2Total() {
    BigDecimal expected = new BigDecimal("60.50");

    this.testItemTotal(1, expected);
  }

  @Test
  public void testItem3Total() {
    BigDecimal expected = new BigDecimal("11.50");

    this.testItemTotal(2, expected);
  }

  @Test
  public void testItem4Total() {
    BigDecimal expected = new BigDecimal("997.99");

    this.testItemTotal(3, expected);
  }
}
