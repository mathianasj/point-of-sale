package com.jsm.pos.entity;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImpl;
import com.jsm.pos.populator.ReceiptPopulator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class AbstractBasketLoader {
  ReceiptPopulator<String> receiptPopulator;
  ObjectMapper objectMapper;
  protected Receipt receipt;
  private final File file;

  @Before
  public void load() throws IOException {
    objectMapper = new ObjectMapper();
    receiptPopulator = new JsonStringReceiptPopulatorImpl(objectMapper);
    receipt = receiptPopulator.populate(FileUtils.readFileToString(file));
  }
}
