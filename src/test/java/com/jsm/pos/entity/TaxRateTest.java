package com.jsm.pos.entity;

import static com.jsm.pos.entity.TestUtil.assertValidation;
import org.junit.Test;

public class TaxRateTest {
  @Test
  public void requireTaxRate() {
    TaxRate underTest = new TaxRate();

    assertValidation(underTest, "taxRate", TestUtil.NOT_NULL);
  }
}
