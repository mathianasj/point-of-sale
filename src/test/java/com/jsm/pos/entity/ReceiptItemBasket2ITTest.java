package com.jsm.pos.entity;

import java.io.File;
import java.math.BigDecimal;
import org.junit.Test;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class ReceiptItemBasket2ITTest extends ReceiptItemBasketITTest {

  public ReceiptItemBasket2ITTest() {
    super(new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket2.json").getFile()));
  }

  @Test
  public void testItem1SubTotal() {
    BigDecimal expected = new BigDecimal("11.00");

    this.testItemSubTotal(0, expected);
  }

  @Test
  public void testItem1Total() {
    BigDecimal expected = new BigDecimal("11.55");

    this.testItemTotal(0, expected);
  }

  @Test
  public void testItem2SubTotal() {
    BigDecimal expected = new BigDecimal("15001.25");

    this.testItemSubTotal(1, expected);
  }

  @Test
  public void testItem2Total() {
    BigDecimal expected = new BigDecimal("17251.50");

    this.testItemTotal(1, expected);
  }
}
