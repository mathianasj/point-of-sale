package com.jsm.pos.entity;

import java.io.File;
import java.math.BigDecimal;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class ReceiptBasket1ITTest extends ReceiptITTest {

  public ReceiptBasket1ITTest() {
    super(new BigDecimal("10.00"), new BigDecimal("116.98"), new BigDecimal("126.98"),
        new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket1.json").getFile()));
  }
}
