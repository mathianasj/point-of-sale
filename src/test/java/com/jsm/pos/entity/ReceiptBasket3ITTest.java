package com.jsm.pos.entity;

import java.io.File;
import java.math.BigDecimal;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class ReceiptBasket3ITTest extends ReceiptITTest {
  public ReceiptBasket3ITTest() {
    super(new BigDecimal("10.80"), new BigDecimal("1138.98"), new BigDecimal("1149.78"),
        new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket3.json").getFile()));
  }
}
