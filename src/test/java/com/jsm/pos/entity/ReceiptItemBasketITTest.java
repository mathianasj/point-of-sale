package com.jsm.pos.entity;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

public abstract class ReceiptItemBasketITTest extends AbstractBasketLoader {
  public ReceiptItemBasketITTest(File file) {
    super(file);
  }

  public void testItemBigDecimal(Function<ReceiptItem, BigDecimal> entry, int position,
      BigDecimal expected) {
    BigDecimal actual =
        entry.apply(receipt.getItems().get(position)).setScale(2, RoundingMode.HALF_UP);
    assertEquals(expected, actual);
  }

  public void testItemSubTotal(int position, BigDecimal expected) {
    this.testItemBigDecimal(e -> e.getSubTotal(), position, expected);
  }

  public void testItemTotal(int position, BigDecimal expected) {
    this.testItemBigDecimal(e -> e.getTotal(), position, expected);
  }

  public void testItemTax(int position, BigDecimal expected) {
    this.testItemBigDecimal(e -> e.getTax(), position, expected);
  }

}
