package com.jsm.pos.entity;

import static com.jsm.pos.entity.TestUtil.assertValidation;
import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

@PrepareForTest({Product.class, TaxRate.class})
public class ProductTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void requirePrice() {
    Product underTest = new Product();

    assertValidation(underTest, "price", TestUtil.NOT_NULL);
  }

  @Test
  public void requiresValidTaxRate() {
    Product underTest = new Product();
    TaxRate taxRate = PowerMockito.mock(TaxRate.class);

    underTest.setTaxRates(Arrays.asList(taxRate));

    Mockito.doThrow(new RuntimeException("taxrate invalid")).when(taxRate).validate();

    thrown.expect(Exception.class);
    thrown.expectMessage("taxrate invalid");

    underTest.getSalesTax();
  }

  @Test
  public void salesTaxOneTaxTimesPrice() {
    TaxRate taxRate = Mockito.mock(TaxRate.class);
    Product product = new Product("test item", new BigDecimal("5"), Arrays.asList(taxRate));

    Mockito.when(taxRate.getTaxRate()).thenReturn(new BigDecimal("0.1"));
    Mockito.when(taxRate.getRoundingMultiplier()).thenReturn(new BigDecimal("0.05"));

    BigDecimal expected = new BigDecimal(".50");
    BigDecimal actual = product.getSalesTax();

    assertEquals(expected, actual);
  }

  @Test
  public void aggregateSalesTaxesTimesPrice() {
    TaxRate taxRate = Mockito.mock(TaxRate.class);
    TaxRate taxRate2 = Mockito.mock(TaxRate.class);
    Product product =
        new Product("test item", new BigDecimal("15001.25"), Arrays.asList(taxRate, taxRate2));

    Mockito.when(taxRate.getTaxRate()).thenReturn(new BigDecimal("0.1"));
    Mockito.when(taxRate.getRoundingMultiplier()).thenReturn(new BigDecimal("0.05"));
    Mockito.when(taxRate2.getTaxRate()).thenReturn(new BigDecimal("0.05"));
    Mockito.when(taxRate2.getRoundingMultiplier()).thenReturn(new BigDecimal("0.05"));

    BigDecimal expected = new BigDecimal("2250.25");
    BigDecimal actual = product.getSalesTax();

    assertEquals(expected, actual);
  }
}
