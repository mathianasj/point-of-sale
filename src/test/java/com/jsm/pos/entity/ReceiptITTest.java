package com.jsm.pos.entity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import org.junit.Test;

public abstract class ReceiptITTest extends AbstractBasketLoader {
  private final BigDecimal tax;
  private final BigDecimal subTotal;
  private final BigDecimal total;

  public ReceiptITTest(BigDecimal tax, BigDecimal subTotal, BigDecimal total, File file) {
    super(file);
    this.tax = tax;
    this.subTotal = subTotal;
    this.total = total;
  }

  @Test
  public void salesTaxIsCorrect() {
    BigDecimal expected = tax;
    BigDecimal actual = receipt.getTax().setScale(2, RoundingMode.HALF_UP);

    assertEquals(expected, actual);
  }

  @Test
  public void subTotalIsCorrect() {
    BigDecimal expected = subTotal;
    BigDecimal actual = receipt.getSubTotal().setScale(2, RoundingMode.HALF_UP);

    assertEquals(expected, actual);
  }

  @Test
  public void totalIsCorrect() {
    BigDecimal expected = total;
    BigDecimal actual = receipt.getTotal().setScale(2, RoundingMode.HALF_UP);

    assertEquals(expected, actual);
  }

  public void validateReceiptItemsTotal(List<ReceiptItem> expectedItems) {
    assertThat(receipt.getItems()).usingElementComparator(new CompareItemsTotal())
        .containsOnly(expectedItems.toArray(new ReceiptItem[expectedItems.size()]));
  }

  class CompareItemsTotal implements Comparator<ReceiptItem> {

    @Override
    public int compare(ReceiptItem o1, ReceiptItem o2) {
      return o1.getProduct().getTitle().equals(o2.getProduct().getTitle())
          && o1.getTotal().equals(o2.getTotal()) ? 0 : -1;
    }

  }
}
