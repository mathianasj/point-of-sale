package com.jsm.pos.entity;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;


@PrepareForTest({Receipt.class, ReceiptItem.class})
public class ReceiptTest {
  @Rule
  public PowerMockRule rule = new PowerMockRule();

  @Test
  public void testSubTotalSumAggregateAllItems() {
    Receipt receipt = new Receipt();
    ReceiptItem receiptItem = PowerMockito.mock(ReceiptItem.class);
    ReceiptItem receiptItem2 = PowerMockito.mock(ReceiptItem.class);

    Mockito.when(receiptItem.getSubTotal()).thenReturn(new BigDecimal("2.2"));
    Mockito.when(receiptItem2.getSubTotal()).thenReturn(new BigDecimal("2.2"));

    receipt.setItems(Arrays.asList(receiptItem, receiptItem2));

    BigDecimal expected = new BigDecimal("4.4");
    BigDecimal actual = receipt.getSubTotal();

    assertEquals(expected, actual);
  }

  @Test
  public void testSalesTaxSumAggregateAllItems() {
    Receipt receipt = new Receipt();
    ReceiptItem receiptItem = PowerMockito.mock(ReceiptItem.class);
    ReceiptItem receiptItem2 = PowerMockito.mock(ReceiptItem.class);

    Mockito.when(receiptItem.getTax()).thenReturn(new BigDecimal("2.2"));
    Mockito.when(receiptItem2.getTax()).thenReturn(new BigDecimal("2.3"));

    receipt.setItems(Arrays.asList(receiptItem, receiptItem2));

    BigDecimal expected = new BigDecimal("4.5");
    BigDecimal actual = receipt.getTax();

    assertEquals(expected, actual);
  }

  @Test
  public void testTotalSumAggregateAllItems() {
    Receipt receipt = new Receipt();
    ReceiptItem receiptItem = PowerMockito.mock(ReceiptItem.class);
    ReceiptItem receiptItem2 = PowerMockito.mock(ReceiptItem.class);

    Mockito.when(receiptItem.getTotal()).thenReturn(new BigDecimal("2.2"));
    Mockito.when(receiptItem2.getTotal()).thenReturn(new BigDecimal("2.4"));

    receipt.setItems(Arrays.asList(receiptItem, receiptItem2));

    BigDecimal expected = new BigDecimal("4.6");
    BigDecimal actual = receipt.getTotal();

    assertEquals(expected, actual);
  }
}
