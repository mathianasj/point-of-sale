package com.jsm.pos.entity;

import static org.junit.Assert.assertTrue;
import java.util.stream.Collectors;
import org.hibernate.validator.internal.engine.path.PathImpl;
import com.jsm.pos.exception.ValidationException;

public class TestUtil {
  public static String NOT_NULL = "NotNull";

  public static void assertValidation(BaseEntity e, String fieldName, String constraintMatcher) {
    try {
      e.validate();
    } catch (ValidationException ex) {
      TestUtil.assertFieldError(ex, fieldName, constraintMatcher);
    }
  }

  public static void assertFieldError(ValidationException ex, String fieldName,
      String constraintMatcher) {
    assertTrue(fieldName + " must contain " + constraintMatcher,
        ex.getFailures().stream()
            .filter(e -> e.getMessageTemplate().contains(constraintMatcher)
                && e.getPropertyPath().equals(PathImpl.createPathFromString((fieldName))))
            .collect(Collectors.toList()).size() == 1);
  }
}
