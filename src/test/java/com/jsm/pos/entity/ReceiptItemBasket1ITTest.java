package com.jsm.pos.entity;

import java.io.File;
import java.math.BigDecimal;
import org.junit.Test;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class ReceiptItemBasket1ITTest extends ReceiptItemBasketITTest {

  public ReceiptItemBasket1ITTest() {
    super(new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket1.json").getFile()));
  }

  @Test
  public void testItem1SubTotal() {
    BigDecimal expected = new BigDecimal("16.00");

    this.testItemSubTotal(0, expected);
  }

  @Test
  public void testItem2SubTotal() {
    BigDecimal expected = new BigDecimal("99.99");

    this.testItemSubTotal(1, expected);
  }

  @Test
  public void testItem3SubTotal() {
    BigDecimal expected = new BigDecimal("0.99");

    this.testItemSubTotal(2, expected);
  }

  @Test
  public void testItem1Total() {
    BigDecimal expected = new BigDecimal("16.00");

    this.testItemTotal(0, expected);
  }

  @Test
  public void testItem2Total() {
    BigDecimal expected = new BigDecimal("109.99");

    this.testItemTotal(1, expected);
  }

  @Test
  public void testItem3Total() {
    BigDecimal expected = new BigDecimal("0.99");

    this.testItemTotal(2, expected);
  }

  @Test
  public void testItem1Tax() {
    BigDecimal expected = new BigDecimal("0.00");

    this.testItemTax(0, expected);
  }

  @Test
  public void testItem2Tax() {
    BigDecimal expected = new BigDecimal("10.00");

    this.testItemTax(1, expected);
  }

  @Test
  public void testItem3Tax() {
    BigDecimal expected = new BigDecimal("0.00");

    this.testItemTax(2, expected);
  }
}
