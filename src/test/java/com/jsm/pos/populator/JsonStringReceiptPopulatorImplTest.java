package com.jsm.pos.populator;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.pos.entity.Receipt;

public class JsonStringReceiptPopulatorImplTest {
  ReceiptPopulator<String> receiptPopulator;
  ObjectMapper objectMapper;

  @Before
  public void load() {
    objectMapper = Mockito.mock(ObjectMapper.class);
    receiptPopulator = new JsonStringReceiptPopulatorImpl(objectMapper);
  }

  @Test
  public void mapperReturnsWhenInput()
      throws JsonParseException, JsonMappingException, IOException {
    Receipt expected = new Receipt();
    String input = "input";
    Mockito.when(objectMapper.readValue(input, Receipt.class)).thenReturn(expected);

    Receipt actual = receiptPopulator.populate(input);

    assertEquals(expected, actual);
  }
}
