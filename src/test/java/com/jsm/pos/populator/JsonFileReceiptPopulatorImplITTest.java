package com.jsm.pos.populator;

import static org.junit.Assert.assertNotNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsm.pos.entity.Product;
import com.jsm.pos.entity.Receipt;
import com.jsm.pos.entity.ReceiptItem;
import com.jsm.pos.entity.TaxRate;

public class JsonFileReceiptPopulatorImplITTest {
  ReceiptPopulator<File> receiptPopulator;
  ObjectMapper objectMapper;
  File input;

  @Before
  public void load() throws IOException {
    objectMapper = new ObjectMapper();
    receiptPopulator = new JsonFileReceiptPopulatorImpl(objectMapper);
    input =
        new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket1.json").getFile());
  }

  @Test
  public void instantiated() {
    Receipt receipt = receiptPopulator.populate(input);

    assertNotNull(receipt);
  }

  @Test
  public void containsItems() {
    Receipt expected = new Receipt();
    Receipt receipt = receiptPopulator.populate(input);
    TaxRate salesTaxRate = new TaxRate(new BigDecimal("0.1"), null);

    expected.setItems(new ArrayList<>());
    expected.getItems().add(new ReceiptItem(1,
        new Product("16lb bag of Skittles", new BigDecimal("16.00"), new ArrayList<>())));
    expected.getItems().add(
        new ReceiptItem(1, new Product("Walkman", new BigDecimal("99.99"), new ArrayList<>())));
    expected.getItems().get(1).getProduct().getTaxRates().add(salesTaxRate);
    expected.getItems().add(new ReceiptItem(1,
        new Product("bag of microwave Popcorn", new BigDecimal("0.99"), new ArrayList<>())));


    assertReflectionEquals(expected, receipt);
  }
}
