package com.jsm.pos.formatter;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import com.jsm.pos.entity.Product;
import com.jsm.pos.entity.Receipt;
import com.jsm.pos.entity.ReceiptItem;

@PrepareForTest({Receipt.class})
public class CommandLineFormatterTest {
  @Rule
  public PowerMockRule rule = new PowerMockRule();

  @Test
  public void line1EqualsItem1() {
    PrintStream printStream = PowerMockito.mock(PrintStream.class);
    BigDecimalFormatter bigDecimalFormatter = Mockito.mock(BigDecimalFormatter.class);
    ReceiptFormatter receiptFormatter = new CommandLineFormatter(printStream, bigDecimalFormatter);
    ReceiptItem receiptItem = Mockito.mock(ReceiptItem.class);
    Product product = Mockito.mock(Product.class);
    BigDecimal expectedTotal = new BigDecimal("2.20");
    Receipt receipt = new Receipt();

    receipt.setItems(new ArrayList<>());
    receipt.getItems().add(receiptItem);

    Mockito.when(receiptItem.getProduct()).thenReturn(product);
    Mockito.when(receiptItem.getQty()).thenReturn(1);
    Mockito.when(receiptItem.getTax()).thenReturn(BigDecimal.ZERO);
    Mockito.when(product.getTitle()).thenReturn("product");
    Mockito.when(receiptItem.getTotal()).thenReturn(expectedTotal);
    Mockito.when(bigDecimalFormatter.format(expectedTotal)).thenReturn("2.20");

    receiptFormatter.write(receipt);

    Mockito.verify(printStream).println("1 product: 2.20");
  }

  @Test
  public void subTotalEquals() {
    PrintStream printStream = PowerMockito.mock(PrintStream.class);
    BigDecimalFormatter bigDecimalFormatter = Mockito.mock(BigDecimalFormatter.class);
    ReceiptFormatter receiptFormatter = new CommandLineFormatter(printStream, bigDecimalFormatter);
    BigDecimal expectedTotal = new BigDecimal("2.20");
    Receipt receipt = Mockito.mock(Receipt.class);

    receipt.setItems(new ArrayList<>());

    Mockito.when(receipt.getTax()).thenReturn(expectedTotal);
    Mockito.when(bigDecimalFormatter.format(expectedTotal)).thenReturn("1.2");

    receiptFormatter.write(receipt);

    Mockito.verify(printStream).println("Sales Taxes: 1.2");
  }

  @Test
  public void totalEquals() {
    PrintStream printStream = PowerMockito.mock(PrintStream.class);
    BigDecimalFormatter bigDecimalFormatter = Mockito.mock(BigDecimalFormatter.class);
    ReceiptFormatter receiptFormatter = new CommandLineFormatter(printStream, bigDecimalFormatter);
    BigDecimal expectedTotal = new BigDecimal("2.21");
    Receipt receipt = Mockito.mock(Receipt.class);

    receipt.setItems(new ArrayList<>());

    Mockito.when(receipt.getTotal()).thenReturn(expectedTotal);
    Mockito.when(bigDecimalFormatter.format(expectedTotal)).thenReturn("4.3");

    receiptFormatter.write(receipt);

    Mockito.verify(printStream).println("Total: 4.3");
  }
}
