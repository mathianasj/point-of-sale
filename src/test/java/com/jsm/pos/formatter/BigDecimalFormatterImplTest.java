package com.jsm.pos.formatter;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import org.junit.Test;

public class BigDecimalFormatterImplTest {
  @Test
  public void testDefaultFormatOfTwoStripZeros() {
    BigDecimalFormatter underTest = new BigDecimalFormatterImpl();

    String expected = "1.23";
    String actual = underTest.format(new BigDecimal("1.2300"));

    assertEquals(expected, actual);
  }

  @Test
  public void testDefaultFormatOfTwo() {
    BigDecimalFormatter underTest = new BigDecimalFormatterImpl();

    String expected = "1.20";
    String actual = underTest.format(new BigDecimal("1.2"));

    assertEquals(expected, actual);
  }

  @Test
  public void testDefaultRoundingFormatOfTwoRoundsDown() {
    BigDecimalFormatter underTest = new BigDecimalFormatterImpl();

    String expected = "1.23";
    String actual = underTest.format(new BigDecimal("1.234"));

    assertEquals(expected, actual);
  }

  @Test
  public void testDefaultRoundingFormatOfTwoRoundsUp() {
    BigDecimalFormatter underTest = new BigDecimalFormatterImpl();

    String expected = "1.24";
    String actual = underTest.format(new BigDecimal("1.236"));

    assertEquals(expected, actual);
  }

  @Test
  public void testDefaultCommaAdded() {
    BigDecimalFormatter underTest = new BigDecimalFormatterImpl();

    String expected = "14,147.24";
    String actual = underTest.format(new BigDecimal("14147.236"));

    assertEquals(expected, actual);
  }
}
