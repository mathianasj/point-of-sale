package com.jsm.pos.formatter;

import java.io.File;
import java.io.PrintStream;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import com.jsm.pos.entity.AbstractBasketLoader;
import com.jsm.pos.entity.Receipt;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

@PrepareForTest({Receipt.class})
public class CommandLineFormatterITTest extends AbstractBasketLoader {
  public CommandLineFormatterITTest() {
    super(new File(JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket1.json").getFile()));
  }

  @Rule
  public PowerMockRule rule = new PowerMockRule();

  @Test
  public void outputMatches() {
    PrintStream printStream = PowerMockito.mock(PrintStream.class);
    BigDecimalFormatter bigDecimalFormatter = new BigDecimalFormatterImpl();
    ReceiptFormatter receiptFormatter = new CommandLineFormatter(printStream, bigDecimalFormatter);

    receiptFormatter.write(receipt);

    InOrder orderVerifier = Mockito.inOrder(printStream);

    orderVerifier.verify(printStream).println("1 16lb bag of Skittles: 16.00");
    orderVerifier.verify(printStream).println("1 Walkman: 109.99");
    orderVerifier.verify(printStream).println("1 bag of microwave Popcorn: 0.99");
    orderVerifier.verify(printStream).println("Sales Taxes: 10.00");
    orderVerifier.verify(printStream).println("Total: 126.98");
    orderVerifier.verifyNoMoreInteractions();
  }
}
