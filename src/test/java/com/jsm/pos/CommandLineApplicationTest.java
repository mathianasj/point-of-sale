package com.jsm.pos;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;
import com.jsm.pos.populator.JsonStringReceiptPopulatorImplITTest;

public class CommandLineApplicationTest {
  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }


  @Test
  public void ussagePrintsWhenNoInpuArguments() {
    CommandLineApplication.main(new String[] {});
    String expected = "Point of Sale v1.0\n" + "    usage: CommandLineApp receipt.json\n";
    String actual = outContent.toString();

    assertEquals(expected, actual);
  }

  @Test
  public void basket1OutputExpected() {
    CommandLineApplication.main(new String[] {
        JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket1.json").getFile()});
    String expected = "1 16lb bag of Skittles: 16.00\n" + "1 Walkman: 109.99\n"
        + "1 bag of microwave Popcorn: 0.99\n" + "Sales Taxes: 10.00\n" + "Total: 126.98\n";
    String actual = outContent.toString();

    assertEquals(expected, actual);
  }

  @Test
  public void basket2OutputExpected() {
    CommandLineApplication.main(new String[] {
        JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket2.json").getFile()});
    String expected = "1 imported bag of Vanilla-Hazelnut Coffee: 11.55\n"
        + "1 imported Vespa: 17,251.50\n" + "Sales Taxes: 2,250.80\n" + "Total: 17,263.05\n";
    String actual = outContent.toString();

    assertEquals(expected, actual);
  }

  @Test
  public void basket3OutputExpected() {
    CommandLineApplication.main(new String[] {
        JsonStringReceiptPopulatorImplITTest.class.getResource("/Basket3.json").getFile()});
    String expected = "1 imported crate of Almond Snickers: 79.79\n" + "1 Discman: 60.50\n"
        + "1 Imported Bottle of Wine: 11.50\n" + "1 300# bag of Fair-Trade Coffee: 997.99\n"
        + "Sales Taxes: 10.80\n" + "Total: 1,149.78\n";
    String actual = outContent.toString();

    assertEquals(expected, actual);
  }
}
