# Point of Sale

This purpose of this application is to take an input json file that includes product and tax rate information and generate a command line output that represents a typical receipt.  This includes product title, qty, and total with tax and finally an aggregate of both sales tax for all items, and a grand total.

## Prerequesites
* Java SDK 8

## Building
This will compile and run all unit and integration tests
### Mac OS X/Linux
Execute the following commands in a terminal window
```
git clone https://bitbucket.org/mathianasj/point-of-sale.git
cd point-of-sale
chmod +x gradlew && ./gradlew clean build fatJar
```

## Running
### Mac OS X/Linux
#### Prerequesites
The assumption is made that the build actions were completed and successful from the above section
#### Example Basket 1
```
java -jar build/libs/point-of-sale-all.jar src/test/resources/Basket1.json
```
#### Example Basket 2
```
java -jar build/libs/point-of-sale-all.jar src/test/resources/Basket2.json
```
#### Example Basket 3
```
java -jar build/libs/point-of-sale-all.jar src/test/resources/Basket3.json
```